package fr.ulille.iutinfo.teletp;

import android.app.Application;
import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ResourceBundle;

public class SuiviViewModel extends AndroidViewModel {

    private MutableLiveData<String> liveLocalisation;
    private MutableLiveData<String> liveUsername;
    private Integer liveNextQuestion;
    private String [] questions;
    // TODO Q2.a

    public SuiviViewModel(Application application) {
        super(application);
        liveLocalisation = new MutableLiveData<>();
        liveUsername = new MutableLiveData<>();
        liveNextQuestion = 0;

    }

    public LiveData<String> getLiveUsername() {
        return liveUsername;
    }

    public void setUsername(String username) {
        liveUsername.setValue(username);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Username : " + username, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getUsername() {
        return liveUsername.getValue();
    }
    public LiveData<String> getLiveLocalisation() {
        return liveLocalisation;
    }

    public void setLocalisation(String localisation) {
        liveLocalisation.setValue(localisation);
        Context context = getApplication().getApplicationContext();
        Toast toast = Toast.makeText(context, "Localisation : " + localisation, Toast.LENGTH_LONG);
        toast.show();
    }

    public String getLocalisation() {
        return liveLocalisation.getValue();
    }
    
    // TODO Q2.a*
    public void initQuestion(Context context){
        questions= context.databaseList();
    }
    public String getQuestions(int position){
        if(position>=0 && position<questions.length-1) {
            return questions[position];
        }
        return null;
    }
    public LiveData<Integer> getLiveNextQuestion(){
        //return liveNextQuestion;
        return null;
    }
    public void setNextQuestion(Integer nextQuestion){
        this.liveNextQuestion=nextQuestion;
    }
    public Integer getNextQuestion(){
        return liveNextQuestion;
    }
}
