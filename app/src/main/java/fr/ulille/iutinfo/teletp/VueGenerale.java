package fr.ulille.iutinfo.teletp;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.fragment.NavHostFragment;

public class VueGenerale extends Fragment  {

    // TODO Q1
    String salle;
    String poste;
    String DISTANCIEL;

    // TODO Q2.c
    private SuiviViewModel model;


    public String getSalle() {
        return salle;
    }

    public String getPoste() {
        return poste;
    }

    public String getDISTANCIEL() {
        return DISTANCIEL;
    }


    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.vue_generale, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // TODO Q1

        DISTANCIEL= getResources().getStringArray(R.array.list_salles)[0];
        poste = "";
        salle=DISTANCIEL;


        // TODO Q2.c

        model = new ViewModelProvider(requireActivity()).get(SuiviViewModel.class);
        // TODO Q4
        Spinner spSalle = view.findViewById(R.id.spSalle);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this.getContext(),
                R.array.list_salles, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spSalle.setAdapter(adapter);

        Spinner spPoste = view.findViewById(R.id.spPoste);
        ArrayAdapter<CharSequence> adapterPoste = ArrayAdapter.createFromResource(this.getContext(),
                R.array.list_postes, android.R.layout.simple_spinner_item);
        adapterPoste.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPoste.setAdapter(adapterPoste);

        view.findViewById(R.id.btnToListe).setOnClickListener(view1 -> {
            // TODO Q3
            EditText login = view.findViewById(R.id.tvLogin);
            model.setLocalisation(login.getText().toString());
            NavHostFragment.findNavController(VueGenerale.this).navigate(R.id.generale_to_liste);

        });

        // TODO Q5.b

        update(spPoste);
        // TODO Q9
        TextView textView = view.findViewById(R.id.tvNextQuestion);
        textView.setVisibility(View.VISIBLE);
    }

    // TODO Q5.a
    private void update(Spinner spPoste){
     if(salle.equals("Distanciel")){
        spPoste.setVisibility(View.VISIBLE);
        spPoste.setEnabled(true);
     }else{
         spPoste.setVisibility(View.INVISIBLE);
        }
    }
    // TODO Q9
}